#include <assert.h>
#include <errno.h>
#include <string.h>

#include <pthread.h>
#include <inttypes.h>
#include <stdio.h>
 
#include <sys/time.h>
#include <sys/resource.h> 
#include <sys/syscall.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

#include "linux_timer.h"

void* thread_function(void *);

#define SIGNAL_NUMBER SIGALRM

#define NUM_OF_THREAD 5
#define MAX_CYCLE 20

pthread_mutex_t stdout_mutex = PTHREAD_MUTEX_INITIALIZER;

int done = 0;

int main()
{
	/* In order to permit the creation of threads with SCHED_FIFO policy and a
	 * specified priority for each ones, we must call the setrlimit() function
	 * to set the soft and hard real time resources limits of the system to
	 * RLIM_INFINITY. 
	 * 
	 * Note: This means that only root is able to run this program.
	 */
	struct rlimit resources_limits =
	{
		.rlim_cur = RLIM_INFINITY,
		.rlim_max = RLIM_INFINITY
	};
	if (-1 == setrlimit(RLIMIT_RTPRIO, &resources_limits))
	{
		printf ("Errorat line %d: %d %s \n", __LINE__, errno, strerror(errno));
		return -1;
	}
	
	int status = 0;
	pthread_attr_t thread_attr;
	pthread_attr_init(&thread_attr);
	
	status = pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);
	if (0 != status)
	{
		printf ("Errorat line %d: %d %s \n", __LINE__, status, strerror(status));
		return -1;
	}
	
	status = pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
	if (0 != status)
	{
		printf ("Errorat line %d: %d %s \n", __LINE__, status, strerror(status));
		return -1;
	}
	
	status = pthread_attr_setschedpolicy(&thread_attr, SCHED_FIFO);
	if (0 != status)
	{
		printf ("Errorat line %d: %d %s \n", __LINE__, status, strerror(status));
		return -1;
	}
	
	status = pthread_attr_setscope(&thread_attr, PTHREAD_SCOPE_SYSTEM);
	if (0 != status)
	{
		printf ("Errorat line %d: %d %s \n", __LINE__, status, strerror(status));
		return -1;
	}
	
	pthread_t thread_id[NUM_OF_THREAD];
	int32_t thread_period[NUM_OF_THREAD];
	struct sched_param sched_parameter[NUM_OF_THREAD];
	for (int32_t i = 0; i < NUM_OF_THREAD; i++)
	{
		thread_period[i] = 3000 + i * 1000;
		sched_parameter[i].sched_priority = 70 - i;
		
		status = pthread_attr_setschedparam(&thread_attr, &sched_parameter[i]);
		if (0 != status)
		{
			printf ("Errorat line %d: %d %s \n", __LINE__, status, strerror(status));
			return -1;
		}
		
		status = pthread_create(&thread_id[i], &thread_attr, thread_function, &thread_period[i]);
		if (0 != status)
		{
			printf ("Errorat line %d: %d %s \n", __LINE__, status, strerror(status));
			return -1;
		}
	}
	
	for (int32_t i = 0; i < NUM_OF_THREAD; i++)
	{
		status = pthread_join(thread_id[i], NULL);
		if (0 != status)
		{
			printf ("Errorat line %d: %d %s \n", __LINE__ , status, strerror(status));
			return -1;
		}
	}	

	return 0;
}
 
void* thread_function(void * args)
{
	timer_t timer_descriptor;
	sigset_t signal_set;
	int32_t signal_number = SIGNAL_NUMBER;
	
	
	int32_t * period_ms = (int32_t*) args;
	
	linux_create_timer(
			&timer_descriptor,
			&signal_set,
			signal_number,
			*period_ms);
			
			
	for (int32_t cycle = 0; cycle < MAX_CYCLE; cycle++)
	{
		int64_t overrun = linux_wait_period(
								&timer_descriptor,
								&signal_set);
		
		// Do something!
		
		int thr_id = syscall(SYS_gettid);
		struct timespec timestamp;
		clock_gettime(CLOCK_MONOTONIC, &timestamp);
		pthread_mutex_lock(&stdout_mutex);
		clock_gettime(CLOCK_MONOTONIC, &timestamp);
		printf("%lu.%ld thread id:%d overrun: %lu\n",
				timestamp.tv_sec,
				timestamp.tv_nsec,
				thr_id,
				overrun);
		
		pthread_mutex_unlock(&stdout_mutex);
	}
	
	done ++;
	
	return NULL;
}


 
 
