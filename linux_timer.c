#include "linux_timer.h"

#include <sys/syscall.h>
#include <unistd.h>
 
void linux_create_timer(	
		timer_t		* const	timer_descriptor,
		sigset_t	* const	signal_set,
		int32_t 			wake_up_signal_number,
		int32_t 			period_ms)
{
	struct sigevent sevent;
	sevent.sigev_notify = SIGEV_SIGNAL | SIGEV_THREAD_ID;
	sevent.sigev_signo = wake_up_signal_number;
	sevent._sigev_un._tid = syscall(__NR_gettid);
	
	
	struct itimerspec new_value, old_value;
	timer_create (CLOCK_MONOTONIC, &sevent, timer_descriptor);
	new_value.it_interval.tv_sec = period_ms / 1000;	
	new_value.it_value.tv_sec = period_ms / 1000;
	new_value.it_interval.tv_nsec = (period_ms % 1000) * 1000;
	new_value.it_value.tv_nsec = (period_ms % 1000) * 1000;	
	timer_settime(*timer_descriptor, 0, &new_value, &old_value);
	
	sigemptyset(signal_set);
	sigaddset(signal_set, wake_up_signal_number);
	sigprocmask(SIG_BLOCK, signal_set, NULL);
} 

uint64_t linux_wait_period(	
		timer_t		* const	timer_descriptor,
		sigset_t	* 		signal_set)
{
	uint64_t overrun_ticks;
	int32_t	wake_up_signal_number = 0;
	sigwait (signal_set, &wake_up_signal_number);
	overrun_ticks = timer_getoverrun (*timer_descriptor);
	return overrun_ticks;
}

