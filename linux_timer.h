#ifndef LINUX_TIMER_HEADER
#define LINUX_TIMER_HEADER

#include <inttypes.h>
#include <signal.h>
#include <time.h>


void linux_create_timer(	
		timer_t		* const	timer_descriptor,
		sigset_t	* const	signal_set,
		int32_t 			wake_up_signal_number,
		int32_t 			period_ms);
								
uint64_t linux_wait_period(	
		timer_t		* const	timer_descriptor,
		sigset_t	* 		signal_set);

#endif
